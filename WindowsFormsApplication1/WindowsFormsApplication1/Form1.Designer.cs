﻿namespace WindowsFormsApplication1
{
    partial class Igra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tB_Unos = new System.Windows.Forms.TextBox();
            this.enter = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_pokusaji = new System.Windows.Forms.Label();
            this.lbl_Rijec = new System.Windows.Forms.Label();
            this.Exit = new System.Windows.Forms.Button();
            this.newGame = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tB_Unos
            // 
            this.tB_Unos.Location = new System.Drawing.Point(39, 32);
            this.tB_Unos.Name = "tB_Unos";
            this.tB_Unos.Size = new System.Drawing.Size(100, 20);
            this.tB_Unos.TabIndex = 7;
            // 
            // enter
            // 
            this.enter.Location = new System.Drawing.Point(187, 29);
            this.enter.Name = "enter";
            this.enter.Size = new System.Drawing.Size(30, 23);
            this.enter.TabIndex = 8;
            this.enter.Text = "OK";
            this.enter.UseVisualStyleBackColor = true;
            this.enter.Click += new System.EventHandler(this.enter_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Pokušaji:";
            // 
            // lbl_pokusaji
            // 
            this.lbl_pokusaji.AutoSize = true;
            this.lbl_pokusaji.Location = new System.Drawing.Point(151, 73);
            this.lbl_pokusaji.Name = "lbl_pokusaji";
            this.lbl_pokusaji.Size = new System.Drawing.Size(13, 13);
            this.lbl_pokusaji.TabIndex = 10;
            this.lbl_pokusaji.Text = "5";
            // 
            // lbl_Rijec
            // 
            this.lbl_Rijec.AutoSize = true;
            this.lbl_Rijec.Location = new System.Drawing.Point(113, 107);
            this.lbl_Rijec.Name = "lbl_Rijec";
            this.lbl_Rijec.Size = new System.Drawing.Size(26, 13);
            this.lbl_Rijec.TabIndex = 11;
            this.lbl_Rijec.Text = "rijec";
            // 
            // Exit
            // 
            this.Exit.Location = new System.Drawing.Point(197, 155);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(49, 23);
            this.Exit.TabIndex = 12;
            this.Exit.Text = "Exit";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // newGame
            // 
            this.newGame.Location = new System.Drawing.Point(13, 154);
            this.newGame.Name = "newGame";
            this.newGame.Size = new System.Drawing.Size(75, 23);
            this.newGame.TabIndex = 13;
            this.newGame.Text = "New Game";
            this.newGame.UseVisualStyleBackColor = true;
            this.newGame.Click += new System.EventHandler(this.newGame_Click);
            // 
            // Igra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(249, 180);
            this.Controls.Add(this.newGame);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.lbl_Rijec);
            this.Controls.Add(this.lbl_pokusaji);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.enter);
            this.Controls.Add(this.tB_Unos);
            this.Name = "Igra";
            this.Text = "Igra";
            this.Load += new System.EventHandler(this.Igra_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tB_Unos;
        private System.Windows.Forms.Button enter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_pokusaji;
        private System.Windows.Forms.Label lbl_Rijec;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button newGame;
    }
}

