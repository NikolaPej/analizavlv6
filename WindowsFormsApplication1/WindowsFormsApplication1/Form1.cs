﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Igra : Form
    {

        Random rand = new Random();
        List<string> list =new List<string>();
        int pokusaji;
        string trazenaRijec, igracevaRijec;         //trazena rijec je rijec iz datoteke, a igraceva rijec je ona koja se ispisuje na ekranu

        public Igra()
        {
            InitializeComponent();
        }

                
        public void New_Game()         //prilikom pokretanja aplikacije i nakon svake zavrsene igre (dobivene ili izgubljene) poziva se funkcija New_Game koja zapocinje novu igru
        {
                //ucitavanje nove rijeci iz liste
            trazenaRijec = list[rand.Next(0, list.Count - 1)];          
                //ispis onoliko * na label koliko trazena rijec ima slova
            igracevaRijec = new string('*', trazenaRijec.Length);       
            lbl_Rijec.Text = igracevaRijec;
                //postavljanje broja pokusaja na 5
            pokusaji = 5;
            lbl_pokusaji.Text = pokusaji.ToString();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        

        private void Igra_Load(object sender, EventArgs e)
        {
            //ucitavanje rijeci iz datoteke
            string line;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@"C:\Users\Windows\Desktop\LV6\WindowsFormsApplication1\WindowsFormsApplication1\Rijeci.txt"))  
            {
                while ((line = reader.ReadLine()) != null)
                    {
                        list.Add(line);
                    }
                New_Game();
            }

        }

        private void enter_Click(object sender, EventArgs e)        
        {   
            //unos 1 slova
            if (tB_Unos.Text.Length == 1)
            {
                if (trazenaRijec.Contains(tB_Unos.Text))                    //provjeravamo je li uneseno slovo postoji u rijeci koja se trazi
                {
                    string rijec = trazenaRijec;                            //pravimo neku privremenu varijablu i u nju spremamo nasu trazenu rijec
                    
                    while (rijec.Contains(tB_Unos.Text))
                    {
                        int i = rijec.IndexOf(tB_Unos.Text);                //nova varijabla i, oznacuje mjesto na kojoj se nalazi nase uneseno slovo, u trazenoj rijeci
                        
                        //mjenje * u pogođeno slovo na indexu i
                        StringBuilder rijecx = new StringBuilder (rijec);
                        rijecx[i] = '*';
                        rijec = rijecx.ToString();

                        StringBuilder rijecx1 = new StringBuilder(igracevaRijec);
                        rijecx1[i] = Convert.ToChar(tB_Unos.Text);
                        igracevaRijec = rijecx1.ToString();
                    }
                    
                    lbl_Rijec.Text = igracevaRijec;                         //ispisuje na labelu igracevu rijec, odnosno slovo koje je pogođeno
                    
                    if (igracevaRijec == trazenaRijec)                      //ako korisnik pogodi svako slovo, i pronađe rijec, igra uspjesnoo zavrsava i krece nova igra
                    {
                        MessageBox.Show("Čestitam!! Winn!");
                        New_Game();
                    }
                }
                else                                                        //slucaj kada uneseno slovo nije u rijeci koja se trazi
                {
                    pokusaji--;                                             //smanjujemo broj pokusaja
                    lbl_pokusaji.Text = pokusaji.ToString();
                    
                    if (pokusaji <= 0)                                      //kada korisnik ostane bez pokusaja, igra zavrsa neuspjesno, i krece nova igra
                    {   
                        MessageBox.Show("Game Over!", "You Lose");
                        New_Game();
                    }
                }
            }
            //unos citave rijeci
            else if (tB_Unos.Text.Length > 1)                               
            {
                if (trazenaRijec == tB_Unos.Text)                   //korisnik moze probadi unjeti citavu rijec, te ako pogodi, igra uspjesna zavrsava i krece nova igra
                {
                    MessageBox.Show("Čestitam!! Winn!");
                    New_Game();
                }

                else                                                    //ako ne pogodi, broj pokusaja se smanjuje
                {
                    pokusaji--;
                    lbl_pokusaji.Text = pokusaji.ToString();

                    if (pokusaji <= 0)                                  //kada korisnik ostane bez pokusaja, igra zavrsa neuspjesno, i krece nova igra
                    {
                        MessageBox.Show("Game Over!", "You Lose");
                        New_Game();
                    }
                }
            }
        }

        private void newGame_Click(object sender, EventArgs e)   //newGame button ima istu funkcionalnost kao New_Game funkcija, 
                                                                 //samo sto omogućava korisniku da sam pokrene novu igru bez zavrsetka trenutne
        {
            
            
                trazenaRijec = list[rand.Next(0, list.Count - 1)];
                igracevaRijec = new string('*', trazenaRijec.Length);
                lbl_Rijec.Text = igracevaRijec;
                pokusaji = 5;
                lbl_pokusaji.Text = pokusaji.ToString();
            
        }




    }
}
