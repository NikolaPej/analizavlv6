﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {


        double a, b, rezultat;                                      //a i b predstavljaju varijable u koju ce se spremati brojeci iz dva text boxa, a rezultat je rezultat opeacija nad a i b
        public Form1()
        {
            InitializeComponent();
        }

        //button oduzimanje oduzima vrijednost u prvom text boxu (a) od vrijednosti u drugom text boxu(b)
        private void oduzimanje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox.Text, out a) || !double.TryParse(txtBox1.Text, out b)) // provjera ispravnog unosa brojeva, odnosno provjeravamo da li je uneseno slovo umjesto broja
            {
                MessageBox.Show("Samo brojevi!!");                                              // u tom slucaju korisnika upozoravamo na gresku
                lbl_rez.Text = "Error";
            }
            else
            {
                rezultat = a - b;                        //oduzimanje
                lbl_rez.Text = rezultat.ToString();      //ispis rezultata
                znak.Text = "-";                         //znak i znak1 su dvije labele napravljene radi ljepseg izgleda aplikacije, ispisuju znak operacije koja se koristi između dva text boxa
                znak1.Text = "";
            }
        }

        //buttno zbrajanje sluzi za zbrajanje a i b
        private void Zbrajanje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox.Text, out a) || !double.TryParse(txtBox1.Text, out b))
            {
                MessageBox.Show("Samo brojevi!!");
                lbl_rez.Text = "Error";
            }
            else
            {
                rezultat = a + b;
                lbl_rez.Text = rezultat.ToString();
                znak.Text = "+";
                znak1.Text = "";
            }
        }

        //mnozenje a i b
        private void mnozenje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox.Text, out a) || !double.TryParse(txtBox1.Text, out b))
            {
                MessageBox.Show("Samo brojevi!!");
                lbl_rez.Text = "Error";
            }
            else
            {
                rezultat = a * b;
                lbl_rez.Text = rezultat.ToString();
                znak.Text = "x";                      // ispisuje x koa znak za mnozenjem jer * se bas i ne vidi na aplikaciji
                znak1.Text = "";
            }
        }

        //djeljenje a i b
        private void djeljenje_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox.Text, out a) || !double.TryParse(txtBox1.Text, out b))
            {
                MessageBox.Show("Samo brojevi!!");
                lbl_rez.Text = "Error";
            }
            else
            {
                rezultat = a / b;
                lbl_rez.Text = rezultat.ToString();
                znak.Text = "/";
                znak1.Text = "";
            }
        }

        //racunanje sinusa nekog kuta
        private void sinus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox.Text, out a))
            {
                MessageBox.Show("Samo brojevi!!");
                lbl_rez.Text = "Error";
            }
            else
            {
                double aRad = a * (Math.PI / 180);      //pretvaranje unesenog kuta u radijane
                rezultat = Math.Sin(aRad);              //koristimo predefiniranu funkciju za sinus
                lbl_rez.Text = rezultat.ToString();
                znak1.Text = "sin";
                znak.Text = "x";                        //postavljanje labela između text boxa na mnozenje i
                txtBox1.Text = "1";                     //postavljanje drugog textboxa na 1, radi ljepseg izgleda, tj. da se koriste oba textboxa
            }
        }

        //racunanje kosinusa nekog kuta
        private void cosinus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox.Text, out a))
            {
                MessageBox.Show("Samo brojevi!!");
                lbl_rez.Text = "Error";
            }
            else
            {
                double aRad = a * (Math.PI / 180);      //pretvaranje unesenog kuta u radijane
                rezultat = Math.Cos(aRad);                 //koristimo predefiniranu funkciju za kosinus
                lbl_rez.Text = rezultat.ToString();
                znak1.Text = "cos";
                znak.Text = "x";
                txtBox1.Text = "1";
            }
        }

        //racunanje potencije a^b
        private void potencija_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox.Text, out a) || !double.TryParse(txtBox1.Text, out b))
            {
                MessageBox.Show("Samo brojevi!!");
                lbl_rez.Text = "Error";
            }
            else
            {
                rezultat = Math.Pow(a, b);              //koristimo predefiniranu funkciju za potenciranje
                lbl_rez.Text = rezultat.ToString();
                znak.Text = "^";
                znak1.Text = "";
            }
        }


        //korjen iz nekog broja
        private void korijen_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox.Text, out a))
            {
                MessageBox.Show("Samo brojevi!!");
                lbl_rez.Text = "Error";
            }
            else
            {
                rezultat = Math.Sqrt(a);                //koristimo predefiniranu funkciju za korjenovanje
                lbl_rez.Text = rezultat.ToString();
                znak1.Text = "sqrt";
                znak.Text = "x";                    //postavljanje labela između text boxa na mnozenje i
                txtBox1.Text = "1";                 //postavljanje drugog textboxa na 1, radi ljepseg izgleda, tj. da se koriste oba textboxa
            }
        }


        //racunanje prirodnog logaritma (logaritam sa bazom e)
        private void logaritam_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(txtBox.Text, out a))
            {
                MessageBox.Show("Samo brojevi!!");
                lbl_rez.Text = "Error";
            }
            else
            {
                rezultat = Math.Log(a);                 //koristimo predefiniranu funkciju za logaritam
                lbl_rez.Text = rezultat.ToString();
                znak1.Text = "ln";
                znak.Text = "x";
                txtBox1.Text = "1";
            }
        }

    }
}
