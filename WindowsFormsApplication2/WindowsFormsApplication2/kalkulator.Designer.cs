﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Zbrajanje = new System.Windows.Forms.Button();
            this.oduzimanje = new System.Windows.Forms.Button();
            this.mnozenje = new System.Windows.Forms.Button();
            this.djeljenje = new System.Windows.Forms.Button();
            this.sinus = new System.Windows.Forms.Button();
            this.cosinus = new System.Windows.Forms.Button();
            this.potencija = new System.Windows.Forms.Button();
            this.korijen = new System.Windows.Forms.Button();
            this.logaritam = new System.Windows.Forms.Button();
            this.lbl_rez = new System.Windows.Forms.Label();
            this.txtBox = new System.Windows.Forms.TextBox();
            this.txtBox1 = new System.Windows.Forms.TextBox();
            this.znak = new System.Windows.Forms.Label();
            this.znak1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Zbrajanje
            // 
            this.Zbrajanje.Location = new System.Drawing.Point(12, 109);
            this.Zbrajanje.Name = "Zbrajanje";
            this.Zbrajanje.Size = new System.Drawing.Size(47, 23);
            this.Zbrajanje.TabIndex = 1;
            this.Zbrajanje.Text = "+";
            this.Zbrajanje.UseVisualStyleBackColor = true;
            this.Zbrajanje.Click += new System.EventHandler(this.Zbrajanje_Click);
            // 
            // oduzimanje
            // 
            this.oduzimanje.Location = new System.Drawing.Point(65, 109);
            this.oduzimanje.Name = "oduzimanje";
            this.oduzimanje.Size = new System.Drawing.Size(43, 23);
            this.oduzimanje.TabIndex = 2;
            this.oduzimanje.Text = "-";
            this.oduzimanje.UseVisualStyleBackColor = true;
            this.oduzimanje.Click += new System.EventHandler(this.oduzimanje_Click);
            // 
            // mnozenje
            // 
            this.mnozenje.Location = new System.Drawing.Point(114, 108);
            this.mnozenje.Name = "mnozenje";
            this.mnozenje.Size = new System.Drawing.Size(46, 23);
            this.mnozenje.TabIndex = 4;
            this.mnozenje.Text = "x";
            this.mnozenje.UseVisualStyleBackColor = true;
            this.mnozenje.Click += new System.EventHandler(this.mnozenje_Click);
            // 
            // djeljenje
            // 
            this.djeljenje.Location = new System.Drawing.Point(163, 108);
            this.djeljenje.Name = "djeljenje";
            this.djeljenje.Size = new System.Drawing.Size(47, 23);
            this.djeljenje.TabIndex = 5;
            this.djeljenje.Text = "/";
            this.djeljenje.UseVisualStyleBackColor = true;
            this.djeljenje.Click += new System.EventHandler(this.djeljenje_Click);
            // 
            // sinus
            // 
            this.sinus.Location = new System.Drawing.Point(13, 139);
            this.sinus.Name = "sinus";
            this.sinus.Size = new System.Drawing.Size(34, 23);
            this.sinus.TabIndex = 6;
            this.sinus.Text = "sin";
            this.sinus.UseVisualStyleBackColor = true;
            this.sinus.Click += new System.EventHandler(this.sinus_Click);
            // 
            // cosinus
            // 
            this.cosinus.Location = new System.Drawing.Point(54, 139);
            this.cosinus.Name = "cosinus";
            this.cosinus.Size = new System.Drawing.Size(33, 23);
            this.cosinus.TabIndex = 7;
            this.cosinus.Text = "cos";
            this.cosinus.UseVisualStyleBackColor = true;
            this.cosinus.Click += new System.EventHandler(this.cosinus_Click);
            // 
            // potencija
            // 
            this.potencija.Location = new System.Drawing.Point(95, 139);
            this.potencija.Name = "potencija";
            this.potencija.Size = new System.Drawing.Size(36, 23);
            this.potencija.TabIndex = 8;
            this.potencija.Text = "^";
            this.potencija.UseVisualStyleBackColor = true;
            this.potencija.Click += new System.EventHandler(this.potencija_Click);
            // 
            // korijen
            // 
            this.korijen.Location = new System.Drawing.Point(137, 139);
            this.korijen.Name = "korijen";
            this.korijen.Size = new System.Drawing.Size(35, 23);
            this.korijen.TabIndex = 9;
            this.korijen.Text = "sqrt";
            this.korijen.UseVisualStyleBackColor = true;
            this.korijen.Click += new System.EventHandler(this.korijen_Click);
            // 
            // logaritam
            // 
            this.logaritam.Location = new System.Drawing.Point(176, 137);
            this.logaritam.Name = "logaritam";
            this.logaritam.Size = new System.Drawing.Size(34, 23);
            this.logaritam.TabIndex = 10;
            this.logaritam.Text = "ln";
            this.logaritam.UseVisualStyleBackColor = true;
            this.logaritam.Click += new System.EventHandler(this.logaritam_Click);
            // 
            // lbl_rez
            // 
            this.lbl_rez.AutoSize = true;
            this.lbl_rez.Location = new System.Drawing.Point(111, 65);
            this.lbl_rez.Name = "lbl_rez";
            this.lbl_rez.Size = new System.Drawing.Size(0, 13);
            this.lbl_rez.TabIndex = 11;
            // 
            // txtBox
            // 
            this.txtBox.Location = new System.Drawing.Point(36, 24);
            this.txtBox.Name = "txtBox";
            this.txtBox.Size = new System.Drawing.Size(51, 20);
            this.txtBox.TabIndex = 12;
            // 
            // txtBox1
            // 
            this.txtBox1.Location = new System.Drawing.Point(148, 24);
            this.txtBox1.Name = "txtBox1";
            this.txtBox1.Size = new System.Drawing.Size(51, 20);
            this.txtBox1.TabIndex = 13;
            // 
            // znak
            // 
            this.znak.AutoSize = true;
            this.znak.Location = new System.Drawing.Point(111, 27);
            this.znak.Name = "znak";
            this.znak.Size = new System.Drawing.Size(0, 13);
            this.znak.TabIndex = 14;
            // 
            // znak1
            // 
            this.znak1.AutoSize = true;
            this.znak1.Location = new System.Drawing.Point(12, 27);
            this.znak1.Name = "znak1";
            this.znak1.Size = new System.Drawing.Size(0, 13);
            this.znak1.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "=";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(228, 194);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.znak1);
            this.Controls.Add(this.znak);
            this.Controls.Add(this.txtBox1);
            this.Controls.Add(this.txtBox);
            this.Controls.Add(this.lbl_rez);
            this.Controls.Add(this.logaritam);
            this.Controls.Add(this.korijen);
            this.Controls.Add(this.potencija);
            this.Controls.Add(this.cosinus);
            this.Controls.Add(this.sinus);
            this.Controls.Add(this.djeljenje);
            this.Controls.Add(this.mnozenje);
            this.Controls.Add(this.oduzimanje);
            this.Controls.Add(this.Zbrajanje);
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Zbrajanje;
        private System.Windows.Forms.Button oduzimanje;
        private System.Windows.Forms.Button mnozenje;
        private System.Windows.Forms.Button djeljenje;
        private System.Windows.Forms.Button sinus;
        private System.Windows.Forms.Button cosinus;
        private System.Windows.Forms.Button potencija;
        private System.Windows.Forms.Button korijen;
        private System.Windows.Forms.Button logaritam;
        private System.Windows.Forms.Label lbl_rez;
        private System.Windows.Forms.TextBox txtBox;
        private System.Windows.Forms.TextBox txtBox1;
        private System.Windows.Forms.Label znak;
        private System.Windows.Forms.Label znak1;
        private System.Windows.Forms.Label label1;
    }
}

